module.exports = function (RED) {
  const Log = require('logger');
  const http = require('http');
  const clientSecret = '';

  /**
   * Send IAM request to given endpoint
   *
   * @param {*} endpoint
   * @param {*} payload
   */
  const send = function(endpoint, payload) {
    const self = this;
    return new Promise(async (resolve, reject) => {
      const [ HOST, PORT ] = process.env.IAM_SERVICE_HOST.split(':');
      payload = new URLSearchParams(payload ? payload : {});

      if (!HOST) {
        reject({
          data: 'Invalid http configuration HOST',
          statusCode: -1,
          properties: {
            HOST: HOST,
            PORT: PORT ? PORT : 80
          }
        });
        return;
      }

      if (!endpoint) {
        reject({
          data: 'Invalid endpoint configuration',
          statusCode: -1,
          properties: {
            HOST: HOST,
            PORT: PORT ? PORT : 80
          }
        });
        return;
      }

      const options = {
        hostname: HOST,
        port: PORT ? PORT : 80,
        path: endpoint,
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': payload.toString().length
        }
      }

      const req = http.request(options, function(res) {
        let responseData = '';

        res.on('data', function(chunk) {
          responseData += chunk;
        });

        res.on('end', function() {
          if (res.statusCode > 299) {
            resolve({
              data: null,
              statusCode: res.statusCode
            });
            return;
          }

          responseData = JSON.parse(responseData ? responseData.toString('utf-8') : '{}');

          resolve({
            data: responseData,
            statusCode: res.statusCode
          });
        });

        res.on('error', function (error) {
          reject({
            data: 'HTTP error on response',
            statusCode: res.statusCode,
            properties: {
              error: error
            }
          });
        })
      });

      req.on('error', function(error) {
        reject({
          data: 'HTTP error on request',
          statusCode: -1,
          properties: {
            error: error
          }
        });
      });

      req.write(payload.toString());
      req.end();
    });
  };

  const prepareIamNodeBody = function(data) {
    const iam = {
      userId: '',
      token: data.refresh_token,
      tokenExpiresIn: data.refresh_expires_in,
      session: data.session_state,
      resource_access: {},
      groups: [],
      username: ''
    };
    if (data.access_token) {
        const base64String = data.access_token.split('.')[1];
        const decodedValue = JSON.parse(Buffer.from(base64String, 'base64').toString('ascii'));
        iam.userId = decodedValue.sub;
        iam.resource_access = decodedValue.resource_access;
        iam.groups = decodedValue.groups;
        iam.username = decodedValue.preferred_username;
    }
    return iam;
  };

  /**
   * Handle iam logout event
   *
   * @param {*} config
   */
  function iamLogin(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      // Prepare base configuration
      const iamConfig = {
        token: null,
        username: msg.payload.username || null,
        password: msg.payload.password || null,
        code: msg.payload.code || null,
        redirect_uri: msg.payload.redirect_uri || null
      };

      // If authorization header isset
      if (msg.req?.headers?.authorization) {
        if (msg.req.headers.authorization.indexOf('Basic') > -1) {
          const base64 = msg.req.headers.authorization.replace('Basic ', '');
          const buff = Buffer.from(base64, 'base64');
          const text = buff.toString('utf-8');
          const credentials = text.split(':');
          if (2 === credentials.length) {
            iamConfig.username = credentials[0];
            iamConfig.password = credentials[1];
          }
        }
        if (msg.req.headers.authorization.indexOf('Bearer') > -1) {
          iamConfig.token = msg.req.headers.authorization.replace('Bearer ', '');
        }
      }

      // Prepare request: Code, Token, User+PW
      let payload = {};
      let authType = '';
      if (iamConfig.code) {
        authType = 'code';
        payload = {
          'grant_type': 'authorization_code',
          'client_id': 'account',
          'client_secret': `${clientSecret}`,
          'code': `${iamConfig.code}`,
          'redirect_uri': `${iamConfig.redirect_uri}`,
          'scope': 'openid'
        };

      } else if (iamConfig.token) {
        authType = 'token';
        payload = {
          'grant_type': 'refresh_token',
          'client_id': 'account',
          'client_secret': `${clientSecret}`,
          'refresh_token': `${iamConfig.token}`,
          'scope': 'openid'
        };

      } else {
        authType = 'user+pw';
        payload = {
          'grant_type': 'password',
          'client_id': 'account',
          'client_secret': `${clientSecret}`,
          'username': `${iamConfig.username}`,
          'password': `${iamConfig.password}`,
          'scope': 'openid'
        };
      }

      // Make request
      send(config.endpoint, payload)
        .then(function(res) {
          Log.debug({
            breadcrumbs: ['iam', authType, 'login'],
            message: 'Account logged in',
            properties: {
              httpStatusCode: res.statusCode,
              data: res.data
            }
          });
          msg.iam = prepareIamNodeBody(res.data);
          node.send([msg, null]);
        })
        .catch(function(error) {
          if (!msg)
          Log.error({
            breadcrumbs: ['iam', authType, 'login'],
            message: 'Request error happen',
            properties: error
          });
          node.send([null, msg]);
        });

    });
  }

  /**
   * Handle iam logout event
   *
   * @param {*} config
   */
  function iamLogout(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      const payload = {
        'grant_type': 'refresh_token',
        'client_id': 'account',
        'client_secret': `${clientSecret}`,
        'refresh_token': `${msg.iam.token}`,
        'scope': 'openid'
      };

      send(config.endpoint, payload)
        .then(function(res) {
          Log.debug({
            breadcrumbs: ['iam', 'logout'],
            message: 'Account logged out',
            properties: {
              httpStatusCode: res.statusCode,
              data: res.data
            }
          });
          delete msg.iam;
          node.send([msg, null]);
        })
        .catch(function(error) {
          if (!msg)
          Log.error({
            breadcrumbs: ['iam', 'logout'],
            message: 'Request error happen',
            properties: error
          });
          node.send([null, msg]);
        });

    });
  }

  /**
   * Handle iam logout event
   *
   * @param {*} config
   */
  function iamHasGroup(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      if (!msg.iam || !msg.iam.groups) {
        node.send([null, msg]);
        return;
      }
      const hasGroup = msg.iam.groups.indexOf(config.group) > -1

      if (hasGroup) {
        node.send([msg, null]);
      } else {
        node.send([null, msg]);
      }
    });
  }

  RED.nodes.registerType('iam-login', iamLogin);
  RED.nodes.registerType('iam-logout', iamLogout);
  RED.nodes.registerType('iam-hasgroup', iamHasGroup);
};